<?php
session_start();
$username = "";
$email = "";
$errors = array();
//Database connection
$db=mysqli_connect('localhost','root','','register');

//if the register button is clicked
if (isset($_POST['reg_user'])) {
 // Logic to get form data sent through rest api
  $username = mysqli_real_escape_string($db,$_POST['username']);
  $email = mysqli_real_escape_string($db,$_POST['email']);
  $password = mysqli_real_escape_string($db,$_POST['password']);
  $cpassword = mysqli_real_escape_string($db,$_POST['cpassword']);

//Make sure that form fields are filled properly
  if (empty($username)) {
    array_push($errors,"Username is required");//add error to errors array
  }

  if (empty($email)) {
    array_push($errors,"Email is required");//add error to errors array
  }
  if (empty($password)) {
    array_push($errors,"Password is required");//add error to errors array
  }
  if ($password!=$cpassword) {
    array_push($errors,"Passwords you entered is mismatched");//add error to errors array
  }

  // first check the database to make sure
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM users WHERE username='$username' OR email='$email' LIMIT 1";
  $result = mysqli_query($db, $user_check_query);
  $user = mysqli_fetch_assoc($result);

  if ($user) { // if user exists
    if ($user['username'] === $username) {
      array_push($errors, "Username already exists");
    }

    if ($user['email'] === $email) {
      array_push($errors, "email already exists");
    }
  }

//if there is no error save user to Database
if (count($errors)==0) {
  $password1=md5($password); //encrypt the password before saving in the database
 $sql = "INSERT INTO users(username,email,password) VALUES('$username','$email','$password1')";
mysqli_query($db,$sql);

$_SESSION['username'] = $username;
  	$_SESSION['success'] = "You are now logged in";
  	header('location: homepage.php');
}
}

// LOGIN USER
if (isset($_POST['log_user'])) {
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $password = mysqli_real_escape_string($db, $_POST['password']);

  if (empty($username)) {
  	array_push($errors, "Username is required");
  }
  if (empty($password)) {
  	array_push($errors, "Password is required");
  }

  if (count($errors) == 0) {
  	$password = md5($password);
  	$query = "SELECT * FROM users WHERE username='$username' AND password='$password'";
  	$results = mysqli_query($db, $query);
  	if (mysqli_num_rows($results) == 1) {
  	  $_SESSION['username'] = $username;
  	  $_SESSION['success'] = "You are now logged in";
  	  header('location: homepage.php');
  	}
    else {
  		array_push($errors, "Wrong username/password combination");
  	}
  }
}
 ?>
