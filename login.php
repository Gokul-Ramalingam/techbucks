<?php include('server.php') ?>
<!DOCTYPE html>
<html>
<head>
  <Style>
  .error {
    width: 92%;
    margin: 0px auto;
    padding: 10px;
    border: 1px solid #a94442;
    color: #a94442;
    background: #f2dede;
    border-radius: 5px;
    text-align: left;
  }
  .btn {
  padding: 10px;
  font-size: 15px;
  color: white;
  background: #ef2121;
  border: none;
  border-radius: 5px;
  margin-left: 37%;
  }

  .input-sign {
    margin: 10px 0px 10px 0px;
  }
  .input-sign label {
    display: block;
    text-align: left;
    margin: 3px;
  }
  .input-sign input {
    height: 30px;
    width: 93%;
    padding: 5px 10px;
    font-size: 16px;
    border-radius: 5px;
    border: 1px solid gray;
  }

  </style>
<title>
Log In
</title>
<link rel="stylesheet" type="text/css" href="sigstyle.css">
</head>
<body>
  <div class="header">
    <h4>Log In</h4>
</div>
<form method="post" action="login.php">
  <!--Display validation errors here-->
  <?php include('errors.php'); ?>
  <div class="input-sign">
    <label>Username</label>
    <input type="text" name="username" />
  </div>
  <div class="input-sign">
    <label>Password</label>
    <input type="text" name="password" />
  </div>
  <div class="input-sign">
<button type="submit" class="btn" name="log_user">Login</button>
</div>
<p>
  New to TechCafe? <a href="signup.php">Sign Up</a>
</p>
</form>

</body>
</html>
